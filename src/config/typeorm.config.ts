import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export default {
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'newuser',
  password: 'password',
  database: 'nestjs_jwt',
  entities: ['dist/**/*.entity{.ts,.js}'],
  migrations: ['dist/migrations/*.js'],
  synchronize: false,
  cli: {
    migrationsDir: 'src/migrations',
  },
  logging: true,
} as TypeOrmModuleOptions;
