import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult, DeleteResult } from 'typeorm';
import { CreateUserDto } from './dtos/user.dto';
import { User } from './user.entity';
import { HttpErrorByCode } from '@nestjs/common/utils/http-error-by-code.util';
import { IUser } from './interface/user.interface';
import { UpdateUserDto } from './dtos/user.dto';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}

    async signup(createUserDto: CreateUserDto) {
      return this.userRepository.save(createUserDto);
    }

    async getUserById(userId: number) {
      const user = await this.userRepository.findOne({
          where: {
              id: userId
          }
      });
      if (!user) throw new HttpErrorByCode[404]('User not Found');
      const { id, name,email, createAt } = user;
      return {
        id,
        name,
        email,
        createAt,
      };
    }

    async getUserByEmail(email: string) {
      const user = await this.userRepository.findOne({ where: { email: email } });
      if (!user) throw new HttpErrorByCode[404]('User not Found');
      const { password, id, name, createAt } = user;
      return {
        password,
        id,
        name,
        createAt,
        email,
      };
    }

    async getListUser() {
      const listUser = await this.userRepository.find();
      return listUser.map(({ id, name, email, createAt }) => {
        return { id, name, email, createAt };
      });
    }

    async updateUser(userId: number, req, updateData: UpdateUserDto) {
      const checkUser = await this.getUserById(userId);
      if (!checkUser) throw new HttpErrorByCode[404]('User not found');
      await this.userRepository.update(userId,updateData);
      return {
        success: true,
        message: 'successfully update user',
      };
    }

    async deleteUser(userId: number){
      const checkUser = await this.getUserById(userId);
      if (!checkUser) throw new HttpErrorByCode[404]('User not found');
      await this.userRepository.softDelete(userId);
      return {
        success: true,
        message: 'successfully delete user',
      };
    }


}
