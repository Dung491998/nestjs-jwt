import { Get, HttpCode, Controller,
   UseGuards,ValidationPipe,UsePipes,Param,Put,
   Body,Request, Delete
   } from '@nestjs/common';
import { UsersService } from './users.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { ValidateId } from '../../util/dtos/validateParam.dto';
import { UpdateUserDto } from './dtos/user.dto';
import { IUser } from './interface/user.interface';
import { ApiBearerAuth,ApiParam } from '@nestjs/swagger';

@UseGuards(JwtAuthGuard)
@ApiBearerAuth('defaultBearerAuth')
@Controller('user')
export class UserController {
  constructor(private userService: UsersService) {}

  @Get()
  @HttpCode(200)
  async getListPost() {
    return await this.userService.getListUser();
  }

  @Get(':id')
  @UsePipes(ValidationPipe)
  @ApiParam({name: "id", required: true})
  @HttpCode(200)
  async getPost(@Param() param: ValidateId) {
    return await this.userService.getUserById(param.id);
  }

  @Put(':id')
  @HttpCode(200)
  @UsePipes(ValidationPipe)
  async updateUser(
    @Param() param: ValidateId,
    @Body() updateData: UpdateUserDto,
    @Request() req,
  ) {
    //const user: IUser = await this.userService.getUserById(param.id);
    return await this.userService.updateUser(param.id, req, updateData);
  }

  @Delete(':id')
  @HttpCode(200)
  @UsePipes(ValidationPipe)
  async deletePost(@Param() param: ValidateId) {
    return this.userService.deleteUser(param.id);
  }

}
