import {
    Body,
    Controller,
    Post,
    Request,
    UseGuards,
    UsePipes,
    ValidationPipe,
  } from '@nestjs/common';
  import { AuthGuard } from '@nestjs/passport';
  import { CreateUserDto } from '../users/dtos/user.dto';
  import { UsersService } from '../users/users.service';
  import { AuthService } from './auth.service';
  import { ApiParam, ApiBody } from '@nestjs/swagger';
  @Controller('auth')
  export class AuthController {
    constructor(
      private authService: AuthService,
      private userService: UsersService,
    ) {}
    @UseGuards(AuthGuard('local'))
    @Post('login')
    @ApiParam({name: "password", required: true,description: 'enter your password',type: 'string'})
    @ApiParam({name: "email", required: true,description: 'enter your email',type: 'string'})
    //@ApiBody({description: 'Enter your email and password',type: 'json'})
    //@RouteLogger()
    signin(@Request() req) {
      console.log(req);
      
      return this.authService.login(req.user);
      // return this.userService.signin();
    }
    @Post('register')
    @UsePipes(ValidationPipe)
    signup(@Body() createUserDto: CreateUserDto) {
      return this.userService.signup(createUserDto);
    }
  }
  